import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, IndexRoute, Link } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import Sidebar from './components/sidebar/Sidebar'
import Welcome from './components/welcome/Welcome'
import Forms from './components/forms/Forms'

class App extends React.Component {
  render() {
    return (
      <div>
        <Sidebar />
        {this.props.children}
      </div>
    )
  }
} 


const history = createBrowserHistory()

ReactDOM.render((
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={Welcome} />
      <Route path="forms" component={Forms} />
    </Route>
  </Router>
), document.getElementById('root'))
