import React from 'react'
import Window from './Window'

class Order extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <Window title="Order" handleClose={this.props.handleClose}>
        <div className="inner">
          <div className="ui labeled input">
            <div className="ui label">
              Id:
            </div>
            <input type="text" placeholder="enter id"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              Date:
            </div>
            <input type="text" placeholder="enter date"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              Client Id:
            </div>
            <input type="text" placeholder="enter client id"/>
            <button className="ui button"
                    onClick={this.props.handleOpen.bind(this, 'client')}>More</button>
          </div>
        </div>
      </Window>
    )
  }
}

export default Order
