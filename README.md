### task from email

Итак репозиторий https://bitbucket.org/borgius/dhweb я вышлю вам доступ.

- посмотрите на http://semantic-ui.com/modules/sidebar.html#/definition
- Слева боковая панель, сделайте компонент и внедрите на страницу.
```
Входные данные для компонента
{
"Welcome": "Page URL",
"Forms": "forms"
}
```


- Создать компонент window со стандартным поведением (развернуть, переместить, закрыть)
- за основу можете взять https://github.com/zhbhun/react-bootstrap-window
- и добавить перетаскивание https://github.com/mzabriskie/react-draggable.



- На странице forms сделайте форму Order,
- на ней есть client_id, при нажатии на кнопку рядом с полем открывается окно с формой Client,
- соответственно при нажатии на address_id открывается окно с формой Address.

```
Формы:

  Order:
      id: integer
      date: date
      client_id: integer

  Client:
      client_id: integer
      name: text
      address_id: integer

  Address:
      address_id: integer
      address: text
      city: text
      zip: text
```

### fails
- can't include react-bootstrap-window because it has incompatible version.
