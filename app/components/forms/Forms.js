import React from 'react'
import Order from './Order'
import Client from './Client'
import Address from './Address'

class Forms extends React.Component {
  constructor (props) {
    super(props);

    this.handleClose = (name) => {
      name == 'Client' ? this.setState({client: false})
                       : this.setState({addr: false})
    }

    this.handleOpen = (name) => {
      name == 'client' ? this.setState({client: true})
                       : this.setState({addr: true})
    }

    this.state = {
      client: false,
      addr: false
    }
  }

  render () {
    return (
      <div style={{marginLeft: 250}}>
        <Order handleClose={this.handleClose}
               handleOpen={this.handleOpen}/>
        {this.state.client && <Client handleClose={this.handleClose}
                                      handleOpen={this.handleOpen}/>}
        {this.state.addr && <Address handleClose={this.handleClose} />}
      </div>
    )
  }
}

export default Forms
