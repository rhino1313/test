import React from 'react'
import Window from './Window'

class Client extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <Window title="Client" handleClose={this.props.handleClose}>
        <div className="inner">
          <div className="ui labeled input">
            <div className="ui label">
              Client Id:
            </div>
            <input type="text" placeholder="enter  id"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              Name:
            </div>
            <input type="text" placeholder="enter name"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              Addr Id:
            </div>
            <input type="text" placeholder="enter address"/>
            <button className="ui button"
                    onClick={this.props.handleOpen.bind(this, 'addr')}>More</button>
          </div>
        </div>
      </Window>
    )
  }
}

export default Client
