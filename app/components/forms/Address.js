import React from 'react'
import Window from './Window'

class Address extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <Window title="Address" handleClose={this.props.handleClose}>
        <div className="inner">
          <div className="ui labeled input">
            <div className="ui label">
              Address Id:
            </div>
            <input type="text" placeholder="enter address id"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              Address:
            </div>
            <input type="text" placeholder="enter address"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              City:
            </div>
            <input type="text" placeholder="enter city"/>
          </div>
          <div className="ui labeled input">
            <div className="ui label">
              Zip:
            </div>
            <input type="text" placeholder="enter  zip"/>
          </div>
        </div>
      </Window>
    )
  }
}

export default Address
