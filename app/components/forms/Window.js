import React from 'react'
import Draggable from 'react-draggable'

class Window extends React.Component {
  constructor (props) {
    super(props);

    this.handleClose = () => {
      this.props.handleClose(this.props.title);
    }
  }

  render () {
    return (
      <Draggable zIndex={1000} handle=".handle">
        <div className="window">
          <div className="handle">
            <span>{this.props.title}</span>
            <i className="remove icon"
               onClick={this.handleClose}>
            </i>
          </div>
          {this.props.children}
        </div>
      </Draggable>
    )
  }
}

export default Window
