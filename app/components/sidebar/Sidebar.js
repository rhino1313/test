import React from 'react'
import { Link } from 'react-router'

class Sidebar extends React.Component {
  
  render () {

    var isForms = /forms/.test(window.location.href) ? true : false;

    return (
      <div className="ui sidebar visible inverted vertical menu">
        <Link className={'item ' + (!isForms ? "active" : "")}
              to="/">Welcome</Link>
        <Link className={'item ' + (isForms ? "active" : "")}
              to="/forms">Forms</Link>
      </div>
    )
  }
}

export default Sidebar
